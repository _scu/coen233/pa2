//
//  server.c
//  server
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "constants.h"
#include "common.c"

#define DB_MAX 128

typedef struct dbEntry {
	uint32_t subscriberNo;
	uint8_t technology;
	bool hasPaid;
} database;

void doServerOperation(
	database *db, int dbLen,
	int sockfd, struct sockaddr_in serv_add, socklen_t addr_size
) {
	int n, i, j, l;
	uint8_t buffer[15];
	uint8_t payload[15];
	uint8_t packet[15];
	struct sockaddr_storage serverStorage;

	int clientSegments[256] = {0};

	uint32_t subscriberNo;
	uint16_t start, end, receivedPacketType, accessResponse;
	uint8_t currentClientId, receivedSegmentNo, length, technology;
	bool hasPaid;
	bool userExists;

	for(;;) {
		bzero(buffer, sizeof(buffer));
		n = recvfrom(
			sockfd,
			(uint8_t *)buffer, sizeof(buffer),
			0, (struct sockaddr *) &serverStorage,
			&addr_size);

		buffer[n] = '\0';

		if(n > 0) {
			printPackets(buffer, "Received");
			start = ((uint16_t)buffer[0] << 8) | buffer[1];
			currentClientId = buffer[2];
			receivedPacketType = ((uint16_t) buffer[3] << 8) | buffer[4];
			receivedSegmentNo = (uint8_t) buffer[5];
			length = (uint8_t) buffer[6];

			technology = (uint8_t) buffer[7];
			subscriberNo = ntohl(*(uint32_t *)(buffer + 8));

			end = ((uint16_t)buffer[12] << 8);
			end = end  | buffer[13];


			userExists = false;
			hasPaid = false;

			printf("ClientId: %u, Packet type: %x, segment no: %u, length: %d\n", currentClientId, receivedPacketType, receivedSegmentNo, length);
			printf("Requested Subscriber: %u with %uG technology\n", subscriberNo, technology);

			for(i = 0; i < dbLen; i++) {
				if(db[i].subscriberNo == subscriberNo) {
					if(db[i].technology == technology) {
						userExists = true;
						hasPaid = db[i].hasPaid;
						printf("Subscriber exists\n");
					} else {
						userExists = false;
						printf("Subscriber exists, but has a different technology\n");
					}
					break;
					
				}
			}

			if(userExists) {
				if(hasPaid) {
					accessResponse = ACCESS_OK; 
					printf("Subscriber is permitted to access the network\n");
				} else {
					accessResponse = NOT_PAID;
					printf("Subscriber has not paid!\n");
				}
			} else {
				accessResponse = NOT_EXIST;
				printf("Subscriber does not exists!\n");
			}

			//add start of packet
			packet[0] = (uint8_t) (PACKET_IDENTIFIER >> 8);
			packet[1] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

			packet[2] = (uint8_t) currentClientId;
			
			packet[3] = (uint8_t) (accessResponse >> 8);
			packet[4] = (uint8_t) (accessResponse & 0xFF);

			packet[5] = (uint8_t) receivedSegmentNo;

			packet[6] = (uint8_t) length;
			packet[7] = (uint8_t) technology;

			packet[8] = (uint8_t) buffer[8];
			packet[9] = (uint8_t) buffer[9];
			packet[10] = (uint8_t) buffer[10];
			packet[11] = (uint8_t) buffer[11];

			packet[12] = (uint8_t) (PACKET_IDENTIFIER >> 8);
			packet[13] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

			packet[14] = '\0';
			
			printPackets(packet, "Sending");
			sendto(sockfd, (uint8_t *)packet, sizeof(packet),  
				0, (struct sockaddr *) &serverStorage, 
					addr_size);

			printf("\n");

		}
		
	}
}

int main(int argc, char *argv[]) { 
	int sockfd;
	struct sockaddr_in serv_add;
	socklen_t addr_size;

	int portno;

	if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ) { 
		perror("Socket creation failed");
		exit(EXIT_FAILURE);
	}

	if(argc < 2) {
		printf("DB file path missing in arguments.");
		exit(1);
	} else if(argc < 3) {
		printf("No port provided. Using default port: %d\n", DEFAULT_PORT_NO);
		portno = DEFAULT_PORT_NO;
	} else {
		portno = atoi(argv[2]);
	}

	serv_add.sin_family = AF_INET;
	serv_add.sin_addr.s_addr = INADDR_ANY;
	serv_add.sin_port = htons(portno);


	addr_size = sizeof(serv_add);

	if (bind(sockfd, (const struct sockaddr *) &serv_add, addr_size) < 0 ) { 
		perror("bind failed");
		exit(EXIT_FAILURE);
	}

	char *filepath = argv[1];

	FILE *file;
	file = fopen(filepath, "r");

	database db[DB_MAX];

	if(file) {
		int len = 0;
		char subNo[20];
		int technology, hasPaid;
		uint32_t subscriberNo;

		while(fscanf(file, "%[^,],%d,%d\n", subNo, &technology, &hasPaid) != EOF) {

			subscriberNo = (uint32_t) parseSubscriberNo(subNo, strlen(subNo));
			if (subscriberNo > MAX_SUB_NO || subscriberNo < 0)
			{
				fprintf(stderr, "Skipping invalid number %x, %d, %d\n", subscriberNo, technology, hasPaid);
				continue;
			}
			db[len].subscriberNo = subscriberNo;
			db[len].technology = (uint8_t) technology;
			db[len].hasPaid = (bool) hasPaid;
			len++;


			if (len == DB_MAX) {
				printf("Max database storage reached.");
				break;
			}
		}
		doServerOperation(db, len, sockfd, serv_add, addr_size);
	} else {
		perror("Failed to read the verification file\n");
		exit(1);
	}

	return 0;
} 
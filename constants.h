//
//  constants.h
//  constants
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

#ifndef CONSTANTS_H
#define CONSTANTS_H

#define DEFAULT_PORT_NO 				8085

#define PACKET_IDENTIFIER 				0xFFFF

#define PACKET_TYPE_DATA 				0xFFF1
#define PACKET_TYPE_ACK 				0xFFF2
#define PACKET_TYPE_REJECT 				0xFFF3

#define REJECT_OUT_OF_SEQUENCE 			0xFFF4
#define REJECT_LENGTH_MISMATCH 			0xFFF5
#define REJECT_END_OF_PACKET_MISSING 	0xFFF6
#define REJECT_DUPLICATE 				0xFFF7

#define TECHNOLOGY_2G					0x02
#define TECHNOLOGY_3G					0x03
#define TECHNOLOGY_4G					0x04
#define TECHNOLOGY_5G					0x05

#define ACCESS_PERMISSION				0xFFF8
#define NOT_PAID						0xFFF9
#define NOT_EXIST						0xFFFA
#define ACCESS_OK						0xFFFB

#define MAX_SUB_NO 						0xFFFFFFFF

#endif /* CONSTANTS_H */
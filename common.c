//
//  common.c
//  common
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

void printPackets(uint8_t *packet, char *message) {
	printf("%s: ", message);

	int k = 0;

	while(packet[k] != '\0') {
		printf("%02x ", packet[k++]);
	}
	printf("\n");
}

uint32_t parseSubscriberNo(char *str, int l) {
	uint32_t ret = 0;
	int i;
	for(i = 0; i < l; i++) {
		if(str[i] == '-') {
			continue;
		} else if(str[i] == '\0') {
			return ret;
		} else if(str[i] < '0' || str[i] > '9') {
			return -1;
		}
		ret = ((uint32_t) ret * 10) + (uint32_t)(str[i] - '0');
	}
	return ret;
}
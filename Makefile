build: build_server build_client

build_server:
	gcc -o server server.c

build_client:
	gcc -o client client.c

start_server:
	./server ./Verification_Database.txt

start_client:
	./client
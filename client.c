//
//  client.c
//  client
//
//  Created by Mrithyunjay Jagannath Halinge on 11/17/19.
//  Copyright © 2019 Mrithyunjay Jagannath Halinge. All rights reserved.
//

#define TIMEOUT 3
#define RETRIES	3

#include <stdio.h>
#include <stdlib.h>

#include <string.h>

#include <sys/types.h>
#include <sys/socket.h>

#include <netinet/in.h>

#include "constants.h"
#include "common.c"

int doClientOperation(
	char* subNo, uint8_t technology,
	uint8_t clientId, uint8_t segmentNo, int length,
	int sockfd, struct sockaddr_in serv_add, socklen_t addr_size
) {
	int n;

	uint8_t payload[15];
	uint8_t buffer[15];

	uint32_t subscriberNo;
	uint16_t start, end, accessResponse;
	uint8_t receivedClientId, receivedSegmentNo;

	subscriberNo = (uint32_t) parseSubscriberNo(subNo, 13);

	//add start of packet
	payload[0] = (uint8_t) (PACKET_IDENTIFIER >> 8);
	payload[1] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

	payload[2] = (uint8_t) clientId;
	
	payload[3] = (uint8_t) (ACCESS_PERMISSION >> 8);
	payload[4] = (uint8_t) (ACCESS_PERMISSION & 0xFF);
	payload[5] = (uint8_t) segmentNo;

	payload[6] = (uint8_t) length;
	payload[7] = (uint8_t) technology;

	payload[8] = (uint8_t) (subscriberNo >> 24);;
	payload[9] = (uint8_t) (subscriberNo >> 16);;
	payload[10] = (uint8_t) (subscriberNo >> 8);;
	payload[11] = (uint8_t) (subscriberNo & 0xFF);;

	payload[12] = (uint8_t) (PACKET_IDENTIFIER >> 8);
	payload[13] = (uint8_t) (PACKET_IDENTIFIER & 0xFF);

	printPackets(payload, "Sending");

	sendto(sockfd, (uint8_t *)payload, sizeof(payload), 
		0, (struct sockaddr *) &serv_add, addr_size);

	n = recvfrom(sockfd, (uint8_t *)buffer, sizeof(buffer), 
		0, (struct sockaddr *) &serv_add, &addr_size);

	buffer[n] = '\0';

	if(n > 0) {
		printPackets(buffer, "Received");
		accessResponse = ((uint16_t)buffer[3] << 8) | buffer[4];

		if(accessResponse == ACCESS_OK) {
			printf("Subscriber is permitted to access the network\n");
		} else if(accessResponse == NOT_PAID) {
			printf("Subscriber has not paid!\n");
		} else if(accessResponse == NOT_EXIST) {
			printf("Subscriber does not exist!\n");
		}
	}

	return n;
}

struct scenario {
	char* subscriberNo;
	uint8_t technology;
};

struct scenario scenarioSimulator(int scenario) {
	struct scenario scene;
	switch(scenario) {
		case 1: // Subscriber has access
			scene.subscriberNo = "408-554-6805";
			scene.technology = 04;
			break;

		case 2: // Subscriber has not paid
			scene.subscriberNo = "408-666-8821";
			scene.technology = 03;
			break;

		case 3: // Subscriber technology mismatch
			scene.subscriberNo = "408-680-8821";
			scene.technology = 04;
			break;

		case 4: // Subscriber not found
			scene.subscriberNo = "226-666-6666";
			scene.technology = 05;
			break;

		default:
			printf("NO!!!! NO!!!!!\n");
			break;
	}

	return scene;
	
}

int main(int argc, char *argv[]) { 
	int sockfd;
	struct sockaddr_in serv_add;

	socklen_t addr_size;

	int portno;

	if((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) < 0) { 
		printf("Socket creation failed");
		exit(EXIT_FAILURE);
	}

	if(argc < 2) {
		printf("No port provided. Using default port: %d\n", DEFAULT_PORT_NO);
		portno = DEFAULT_PORT_NO;
	} else {
		portno = atoi(argv[1]);
	}

	serv_add.sin_family = AF_INET;
	serv_add.sin_addr.s_addr = INADDR_ANY;
	serv_add.sin_port = htons(portno);

	addr_size = sizeof(serv_add);

	struct timeval tv;
	tv.tv_sec = TIMEOUT;
	tv.tv_usec = 0;
	setsockopt(sockfd, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof(struct timeval));

	int counter, n, len;

	struct scenario scene;

	uint8_t clientId;
	clientId = 0x0;

	for (;;) {
		printf("++++++ Pick a scenario ++++++\n");
		printf("1. Subscriber has access \n");
		printf("2. Subscriber has not paid \n");
		printf("3. Subscriber technology mismatch\n");
		printf("4. Subscriber not found\n");
		int op;
		scanf("%d", &op);

		scene = scenarioSimulator(op);
		clientId++;
		counter = 0;
		n = -1;
		while(n <= 0 && counter++ < RETRIES) {
			n = doClientOperation(
				scene.subscriberNo, scene.technology,
				clientId, 1, 5,
				sockfd, serv_add, addr_size
			);
			printf("\n");

			if(n <= 0) {
				printf("No response from server for 3 seconds!\n");
				if(counter < RETRIES) {
					printf("Will try again\n");
					printf("Number of retries left: %d\n\n", (RETRIES - counter));
				}
			}
		}
		if(counter >= RETRIES) {
			printf("Server did not respond\n\n");
			exit(EXIT_FAILURE);
		}
	}
	return 0;
}